// Able to click through the # of banners of Days


//Monday
var myMondayIndex = 1;
showMondayDivs(myMondayIndex);

function mondayPlusDivs(n) {
  showMondayDivs(myMondayIndex += n);
}

function mondayCurrentDivs(n) {
  showMondayDivs(myMondayIndex = n);
}

function showMondayDivs(n) {
  var i;
  var x = document.getElementsByClassName("mondaySlides");
  if (n > x.length) {myMondayIndex = 1}
  if (n < 1) {myMondayIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[myMondayIndex-1].style.display = "block";
}

//Tuesday
var myTuesdayIndex = 1;
showTuesdayDivs(myTuesdayIndex);

function tuesdayPlusDivs(n) {
  showTuesdayDivs(myTuesdayIndex += n);
}

function tuesdayCurrentDivs(n) {
  showTuesdayDivs(myTuesdayIndex = n);
}

function showTuesdayDivs(n) {
  var i;
  var x = document.getElementsByClassName("tuesdaySlides");
  if (n > x.length) {myTuesdayIndex = 1}
  if (n < 1) {myTuesdayIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[myTuesdayIndex-1].style.display = "block";
}

//Wednesday
var myWednesdayIndex = 1;
showWednesdayDivs(myWednesdayIndex);

function wednesdayPlusDivs(n) {
  showWednesdayDivs(myWednesdayIndex += n);
}

function wednesdayCurrentDivs(n) {
  showWednesdayDivs(myWednesdayIndex = n);
}

function showWednesdayDivs(n) {
  var i;
  var x = document.getElementsByClassName("wednesdaySlides");
  if (n > x.length) {myWednesdayIndex = 1}
  if (n < 1) {myWednesdayIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[myWednesdayIndex-1].style.display = "block";
}

//Thursday
var myThursdayIndex = 1;
showThursdayDivs(myThursdayIndex);

function thursdayPlusDivs(n) {
  showThursdayDivs(myThursdayIndex += n);
}

function thursdayCurrentDivs(n) {
  showThursdayDivs(myThursdayIndex = n);
}

function showThursdayDivs(n) {
  var i;
  var x = document.getElementsByClassName("thursdaySlides");
  if (n > x.length) {myThursdayIndex = 1}
  if (n < 1) {myThursdayIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[myThursdayIndex-1].style.display = "block";
}

//Friday
var myFridayIndex = 1;
showFridayDivs(myFridayIndex);

function fridayPlusDivs(n) {
  showFridayDivs(myFridayIndex += n);
}

function fridayCurrentDiv(n) {
  showFridayDivs(myFridayIndex = n);
}

function showFridayDivs(n) {
  var i;
  var x = document.getElementsByClassName("fridaySlides");
  if (n > x.length) {myFridayIndex = 1}
  if (n < 1) {myFridayIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[myFridayIndex-1].style.display = "block";
}

//Saturday
var mySaturdayIndex = 1;
showSaturdayDivs(mySaturdayIndex);

function saturdayPlusDivs(n) {
  showSaturdayDivs(mySaturdayIndex += n);
}

function saturdayCurrentDiv(n) {
  showSaturdayDivs(mySaturdayIndex = n);
}

function showSaturdayDivs(n) {
  var i;
  var x = document.getElementsByClassName("saturdaySlides");
  if (n > x.length) {mySaturdayIndex = 1}
  if (n < 1) {mySaturdayIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[mySaturdayIndex-1].style.display = "block";
}

//Sunday
var mySundayIndex = 1;
showSundayDivs(mySundayIndex);

function sundayPlusDivs(n) {
  showSundayDivs(mySundayIndex += n);
}

function sundayCurrentDiv(n) {
  showSundayDivs(mySundayIndex = n);
}

function showSundayDivs(n) {
  var i;
  var x = document.getElementsByClassName("sundaySlides");
  if (n > x.length) {mySundayIndex = 1}
  if (n < 1) {mySundayIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[mySundayIndex-1].style.display = "block";
}


// Transition # of banners of Days


// Monday
var myMondayIndex = 0;
carouselMonday();

function carouselMonday() {
  var i;
  var x = document.getElementsByClassName("mondaySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myMondayIndex++;
  if (myMondayIndex > x.length) {myMondayIndex = 1}    
  x[myMondayIndex-1].style.display = "block";  
  setTimeout(carouselMonday, 7000); // Change image every 7 seconds
}

//Tuesday
var myTuesdayIndex = 0;
carouselTuesday();

function carouselTuesday() {
  var i;
  var x = document.getElementsByClassName("tuesdaySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myTuesdayIndex++;
  if (myTuesdayIndex > x.length) {myTuesdayIndex = 1}    
  x[myTuesdayIndex-1].style.display = "block";  
  setTimeout(carouselTuesday, 7000); // Change image every 7 seconds
}

//Wednesday
var myWednesdayIndex = 0;
carouselWednesday();

function carouselWednesday() {
  var i;
  var x = document.getElementsByClassName("wednesdaySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myWednesdayIndex++;
  if (myWednesdayIndex > x.length) {myWednesdayIndex = 1}    
  x[myWednesdayIndex-1].style.display = "block";  
  setTimeout(carouselWednesday, 7000); // Change image every 7 seconds
}

//Thursday
var myThursdayIndex = 0;
carouselThursday();

function carouselThursday() {
  var i;
  var x = document.getElementsByClassName("thursdaySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myThursdayIndex++;
  if (myThursdayIndex > x.length) {myThursdayIndex = 1}    
  x[myThursdayIndex-1].style.display = "block";  
  setTimeout(carouselThursday, 7000); // Change image every 7 seconds
}

//Friday
var myFridayIndex = 0;
carouselFriday();

function carouselFriday() {
  var i;
  var x = document.getElementsByClassName("fridaySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myFridayIndex++;
  if (myFridayIndex > x.length) {myFridayIndex = 1}    
  x[myFridayIndex-1].style.display = "block";  
  setTimeout(carouselFriday, 7000); // Change image every 7 seconds
}

//Saturday
var mySaturdayIndex = 0;
carouselSaturday();

function carouselSaturday() {
  var i;
  var x = document.getElementsByClassName("saturdaySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  mySaturdayIndex++;
  if (mySaturdayIndex > x.length) {mySaturdayIndex = 1}    
  x[mySaturdayIndex-1].style.display = "block";  
  setTimeout(carouselSaturday, 7000); // Change image every 7 seconds
}

//Sunday
var mySundayIndex = 0;
carouselSunday();

function carouselSunday() {
  var i;
  var x = document.getElementsByClassName("sundaySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  mySundayIndex++;
  if (mySundayIndex > x.length) {mySundayIndex = 1}    
  x[mySundayIndex-1].style.display = "block";  
  setTimeout(carouselSunday, 7000); // Change image every 7 seconds
}